import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import {LoginService } from '../../services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  public email: string;
  public password: string;
  public isLoggedIn: boolean = false;

  constructor(private loginService : LoginService, private toastController: ToastController) {}

  async createAccount() {
    const _this = this;
   await this.loginService.createAccount(this.email, this.password).then((res) => {
    _this.presentToast(res.user.email, res.additionalUserInfo.isNewUser, false);
    _this.isLoggedIn = false;
  }).catch(async function async(error) {
    _this.presentToast(error, true, true);
    _this.isLoggedIn = false;
  });
  }

  async login() {
    const _this = this;
    await this.loginService.login(this.email, this.password).then((res) => {
      _this.presentToast(res.user.email, res.additionalUserInfo.isNewUser, false);
      _this.isLoggedIn = true;
    }).catch(async function async(error) {
      _this.presentToast(error, false, true);
      _this.isLoggedIn = false;
    });    
  }

  async logout() {
    const _this = this;
    await this.loginService.logout().then(() => {
      _this.presentToast('Se cerró sesión correctamente.', false, true)
      _this.isLoggedIn = false
      _this.clearInputs()
    }).catch(async function async(error) {
      _this.presentToast(error, false, true);
      _this.isLoggedIn = false;
    });
  }

  async presentToast(message: string, isNewUser: boolean, isError: boolean) {
    if(isError){
      const toast = await this.toastController.create({
        message: message,
        position: 'bottom',
        duration: 2000
      });
      toast.present();
    } else{
      const toast = await this.toastController.create({
        message: isNewUser == true? 'Usuario '+ message +' creado.':'Has iniciado sesión con ' + message,
        position: 'bottom',
        duration: 2000
      });
      toast.present();
    }
  }

  private clearInputs(){
    this.email = ''
    this.password = ''
  }

}
