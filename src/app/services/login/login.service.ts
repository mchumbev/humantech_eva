import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor() { }

  createAccount(email: string, password: string): Promise<any> {
    return firebase.auth().createUserWithEmailAndPassword(
      email,
      password
    );
  }

  login(email: string, password: string): Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(
      email,
      password
    );
  }

  logout(): Promise<any> {
    return firebase.auth().signOut();
  }

}
