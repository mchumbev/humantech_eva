// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAYIx_tOMLYz0LeqZGOJyUQK03lb2XEQS4",
    authDomain: "humantech-39d05.firebaseapp.com",
    databaseURL: "https://humantech-39d05.firebaseio.com",
    projectId: "humantech-39d05",
    storageBucket: "humantech-39d05.appspot.com",
    messagingSenderId: "282813720822",
    appId: "1:282813720822:web:930c8a2cc8f826f25264d7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
